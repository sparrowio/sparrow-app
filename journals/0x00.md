# 项目初始化

## 目录结构和基础文件

### README.md LICENSE .gitignore

这三个文件是git项目的三个常见的文件，用于项目描述(README.md)，版权声明(LICENSE)，忽略文件(.gitignore)。对于git，不是很熟悉，需要整理一些笔记。

#### README 自述文件
- 文本文件，名字可以是`README`，`README.md`，`README.TXT`, `READ.ME`，文件名通常大写。
- 支持Markdown语法
- GitHub，GitLab等都会识别该文件

值得一提的是，看到不少项目中有图片标签，如![](https://img.shields.io/badge/sparrow-going-green.svg)，在网上查了些资料发现这个是可以自己制作的，[这里](https://shields.io/)。
或者直接使用`https://img.shields.io/badge/<SUBJECT>-<STATUS>-<COLOR>.svg`，将`<SUBJECT>`，`<STATUS>`，`<COLOR>`替换，如`https://img.shields.io/badge/sparrow-going-green.svg`。这样就生成了图片链接，README是支持markdown语法的，所以就可以在README中加入`![](https://img.shields.io/badge/sparrow-going-green.svg)`。效果就像这样：![](https://img.shields.io/badge/sparrow-going-green.svg)

#### LICENSE

这是版权声明及许可，有多种开源版权许可可供选择:
- https://choosealicense.com/ 
- http://choosealicense.online/

可以在上面的网站上得到帮助。

本项目中的源码使用[Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/)协议，其他文档内容使用[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)协议。

### docs

这个文件夹的作用会记录一些项目中的文档和使用过程中产生的一些文档，可能是文档、博客、译文等。

### journals

这里是每个阶段的一些记录，比如本篇为第一篇——项目初始化。

### src

项目源码

### internals

项目中需要用到的一些内部工具源码

### package.json

这个文件虽然比较熟悉，但是细节不太清楚。所以趁此机会看一下[npm文档](https://docs.npmjs.com/all)，笔记会写在`docs/0x00_npm/`中。

### .editorconfig

这个文件是给编辑器或者IDE看的，帮助开发者在不同的编辑器和IDE之间定义和维护统一的代码风格。这个会看看EditorConfig的文档。

### TODO.md

一些想法和计划会列在这个文件中，当然一些计划的取消也会标注。
