# Sparrow App

![](https://img.shields.io/badge/sparrow-going-green.svg)
![](https://img.shields.io/badge/license-MIT-blue.svg)
![](https://img.shields.io/badge/license-CC_BY--NC--SA_4.0-blue.svg)
![](https://img.shields.io/badge/nodejs-latest-green.svg)
![](https://img.shields.io/badge/npm-latest-green.svg)
![](https://img.shields.io/badge/from-2017--09--14-green.svg)
![](https://img.shields.io/badge/impossible-nothing-blue.svg)

> Small as the sparrow is, it possesses all its internal organs.

## WHAT

这个项目发起的本意，不是为了开源，也不是为了分享，就是想记录自己的学习过程和实践过程。

所以，这个项目可能会有一下问题：
- 无聊——没有什么目的性，为了学习或者使用某种框架或库，生拼硬凑出一个场景。
- 杂乱——会涉及到各种东西，主要是围绕前端知识，但是也有可能是其他领域的东西。
- 低级——这个项目主要就是为了学习前端知识，所以可能会有些低级、幼稚，甚至错误的代码、文档和观点，另外，我英文也不好，所以一些文档的翻译也不合适，甚至错误。
- 失礼——可能你会提一些建议和观点，我会尽可能的接受和学习，但也不一定；项目中可能会用到你的代码、图片、文章等，我会尽力尊重作者，注明来源和遵守协议，但可能有疏漏，请原谅、理解并告知，我会及时改正。
- 长期——这是一场持久战。

所以，这个项目是什么，我也不清楚（尤其是现在），可能后面有各种走向。

## AUTHOR

本项目作者声明为`SparrowIO Group`，本项目所声明的版权与许可均以`SparrowIO Group`为主体。

## LICENSE

本项目中的源码使用[Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/)协议，其他文档内容使用[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)协议。
