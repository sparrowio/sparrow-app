# [译]gitignore文档说明

> https://git-scm.com/docs/gitignore

## 名称

gitignore - 指定要忽略的有意不跟踪的文件

## 简介

1. $HOME/.config/git/ignore
2. $GIT_DIR/info/exclude
3. .gitignore

> 译注：这里是配置gitignore的几个文件

## 描述

`gitignore`文件是指定有意不进行版本跟踪的文件，从而让git忽略。已经被Git跟踪过的文件，没有影响，具体可以看看[说明](#说明)。

`gitignore`文件每一行指定一个规则。当确定是否忽略一个路径的时候，Git一般从多处来确定`gitignore`规则，优先级按下面的顺序从高到低（同一优先级内，最后匹配的规则决定结果）：
  
  - 从命令行中读取命令支持的规则。
  - 从文件路径的同级目录或者上级目录中下，读取`.gitignore`文件，上层目录（直到工作目录中的顶层）中的规则会被下层目录（直到包含该文件的目录）中的所覆盖。这些规则的匹配相对于`.gitignore`文件的。工程项目中的`.gitignore`通常会包含对工程构建时生成文件的规则。
  - 从`$GIT_DIR/info/exclude`读取规则。
  - 从`core.excludesFile`配置的文件中读取规则。

把规则放进哪个文件取决于规则怎么使用。

- 需要版本控制并且通过`clone`分发到各仓库的规则（比如，所有的开发者都想忽略的文件），应该写入`.gitignore`文件中。
- 特定于特定仓库但不需要与其他相关库共享的规则，应该写入`$GIT_DIR/info/exclude`中。
- 一个用户向在各种场景下都想Git忽略的规则（比如，用户使用的编辑器产生的备份文件或临时文件）通常写入一个文件，然后通过用户的`~/.gitconfig`配置文件，配置`core.excludesFile`指向该文件。其默认值为`$XDG_CONFIG_HOME/git/ignore`，如果`$XDG_CONFIG_HOME`没有配置或者为空，会使用`$HOME/.config/git/ignore`。

Git提供的底层工具，如*`git ls-files`*和*`git read-tree`*，读取`gitignore`指定规则通过命令行选项或者命令行选项指定的文件。高级别的Git工具，如*`git status`*和*`git add`*，读取规则通过上面指定的源。
 
## 规则

- 空行不匹配任何文件，因此可以做分隔行来增加可读性。
- `#`开始的行被作为注释。在规则第一个字符`#`之前加上反斜杠` \ `会被当做`#`处理。
- 尾部空格会被忽略，除非使用` \ `引用。
- 可选的前缀`!`是用来反规则的。任意匹配的文件在原来的规则下是不包含的，现在就会包含。如果一个文件的父目录被排除了，那么里面的文件是不可能被包含进来的。Git处于性能的考虑不会去遍历排除在外的文件夹，因此对于它们包含的文件任何规则都不会生效，无论定义在哪儿。在规则的第一个字符`!`之前加上` \ `，表明规则第一个字符是文本`!`，比如`\!important!.txt`。
- 如果一个规则是`/`结尾，出于以下的描述会将其删除，但它讲只会匹配目录。换句话说，`foo/`将仅匹配目录`foo`及以下的路径，不会匹配普通文件或者符号链接`foo`（普通文件和符号链接在Git路径匹配的工作模式下看来是一样的）。
- 如果一个规则中没有包含`/`，Git会将其作为shell glob规则来对待，然后通过相对于`.gitignore`文件的路径名来匹配（如果规则不是来自于`.gitignore`文件，将相对于工作目录的顶级目录）。
- 否则的话，Git会将规则作为shell glob以参数`FNM_PATHNAME`通过函数`fnmatch(3)`来处理：路径名中通配符不会匹配`/`。例如，`Documentation/*.html`会匹配`Documentation/git.html`，而不会匹配`Documentation/ppc/ppc.html`或者`tools/perf/Doucmentation/pref.html`。
- 规则开头的`/`会匹配路径名，例如，`/*.c`会匹配`cat-file.c`，但不会匹配`mozilla-sha1/sha1.c`。

连续两个星号`**`对全路径匹配可能有特别的意义：

- 开头的`**`紧接着一个`/`的规则，意味着匹配所有目录。例如，`**/foo`会匹配所有的文件和目录`foo`，与规则`foo`一样。`**/foo/bar`会匹配任意`foo`目录的所有的文件和目录`bar`。
- 尾部的`**`匹配内部的所有文件。例如`abc/**`匹配相对于`.gitignore`文件的目录`abc`下的所有文件，无限层级深度。
- 一个`/`后跟两个`**`在跟一个`/`，匹配0个或多个目录。例如，`a/**/b`匹配`a/b`，`a/x/b`，`a/x/y/b`等等。
- 其他的连续星号是无效的。

## 注意

`gitignore`的目的是确保某些尚未被Git跟踪的文件不被跟踪。如果要终止跟踪一个已被跟踪的文件，使用*`git rm --cached`*。

## 样例

```shell
  $ git status
  [...]
  # Untracked files:
  [...]
  #       Documentation/foo.html
  #       Documentation/gitignore.html
  #       file.o
  #       lib.a
  #       src/internal.o
  [...]
  $ cat .git/info/exclude
  # ignore objects and archives, anywhere in the tree.
  *.[oa]
  $ cat Documentation/.gitignore
  # ignore generated html files,
  *.html
  # except foo.html which is maintained by hand
  !foo.html
  $ git status
  [...]
  # Untracked files:
  [...]
  #       Documentation/foo.html
  [...]
```

另一个例子：
```shell
  $ cat .gitignore
  vmlinux*
  $ ls arch/foo/kernel/vm*
  arch/foo/kernel/vmlinux.lds.S
  $ echo '!/vmlinux*' >arch/foo/kernel/.gitignore
```

第二个`.gitignore`是阻止Git忽略`arch/foo/kernel/vmlinux.lds.S`。

看这个例子，除了一个文件夹`foo/bar`忽略所有的文件（注意`/*`——没有这个`/`，这个通配符也会排除`foo/bar`下的所有）：
```shell
  $ cat .gitignore
  # exclude everything except directory foo/bar
  /*
  !/foo
  /foo/*
  !/foo/bar
```
