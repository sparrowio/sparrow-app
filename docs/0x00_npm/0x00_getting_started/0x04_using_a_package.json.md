# Using a package.json

管理本地安装的npm包最好的方式是创建一个`package.json`文件。

`package.json`文件虽小，但能让你做到很多有用的事情：
1. 项目包依赖的文档
2. 使用`语义化版本号`进行项目包依赖的版本控制
3. 使构建可重复，从而更容易讲代码共享

## 必要条件
一个`package.json`**至少必须**包含以下内容：
- name
    - 全小写
    - 一个单词，没有空格
    - 允许`-`和`_`
- version
    - `x.x.x`的形式
    - 遵守[语义化版本规范](https://docs.npmjs.com/getting-started/semantic-versioning)

比如：
```json
{
  "name": "my-awesome-package",
  "version": "1.0.0"
}
```

## 创建package.json

创建package.json可执行命令：
```bash
> npm init
```

这个命令会发起命令行的几个问题，结束后，会在执行命令的当前目录下生成一个`package.json`。

### --yes参数

通常来说，命令行问答的形式并不适用于每一个人，尤其是已经熟悉并希望快速使用`package.json`的人来说。

你可以通过运行命令`npm init`并使用`--yes`或`-y`参数来得到一个默认的`package.json`：
```bash
> npm init --yes
```
这样的话，命令就不会问任何问题，而是通过提取当前目录的一些信息，得到一个默认的`package.json`。
```bash
> npm init --yes
Wrote to /home/ag_dubs/my_package/package.json:
 
{
  "name": "my_package",
  "description": "",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "https://github.com/ashleygwilliams/my_package.git"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "bugs": {
    "url": "https://github.com/ashleygwilliams/my_package/issues"
  },
  "homepage": "https://github.com/ashleygwilliams/my_package"
}
```

- **name**: 当前目录名字
- **version**: 总是`1.0.0`
- **description**: 从README中提取，或者是空字符串`""`
- **main**: 总是`main.js`
- **scripts**: 默认创建一个空的`test`脚本命令
- **keywords**: 空
- **author**: 空
- **license**: [ISC](https://opensource.org/licenses/ISC)
- **bugs**: 如果有的话，就从当前目录信息提取
- **homepage**: 如果有的话，就从当前目录信息提取

你可以设置一些`init`命令的默认值，如：
```bash
> npm set init.author.email "wombat@npmjs.com"
> npm set init.author.name "ag_dubs"
> npm set init.license "MIT"
```

> NOTE:
>
> 如果`package.json`中没有`description`字段的话，npm会使用REAMDE中的第一行或者README来代替。描述信息有助于别人在npm search中找到你的包，因此一个准备的描述可以使你的包更易于被发现。

### 定制`init`

`init`的执行过程中，生成的信息和提问的问题是完全可以定制的。这是通过自定义`.npm-init.js`来完成的。npm默认从`HOME`目录中查找这个文件，`~/.npm-init.js`。

一个简单的`.npm-init.js`看起来是这个样子的：
```javascript
module.exports = {
  customField: 'Custom Field',
  otherCustomField: 'This field is really cool'
}
```
将这个文件放在`HOME`目录下，执行`npm init`命令，会产生一个类似这样的`package.json`文件：
```javascript
{
  customField: 'Custom Field',
  otherCustomField: 'This field is really cool'
}
```
自定义问题也是可以的，通过使用`prompt`函数。
```javascript
module.exports = prompt("what's your favorite flavor of ice cream buddy?", "I LIKE THEM ALL");
```
了解更多自定义内容，[看这里](https://github.com/npm/init-package-json)。

## 指定依赖包

为了指定你项目中依赖的包，你需要在`package.json`中指定你要用的包。有两种类型的包你可以指定：
- **"dependencies"**: 在生产环境中需要的包
- **"devDependencies"**: 仅在开发和测试时需要用到的包

### 手动编辑`package.json`

你可以手动编辑`package.json`。你需要在package对象中创建一个`dependencies`的属性，类型为对象。这个对象的属性名为你要用到的包，值兼容于你应用的包的`语义化版本号`表达式。

如果你有一些仅用于本地开发的依赖包，你可以按照上面步骤，创建一个`devDependencies`的属性。

比如，项目在生产环境中，会依赖`my_dep`包，版本为主版本为`1`的任意版本，并且在开发时会用到`my_test_frameword`包，版本为主版本为`3`的任意版本：
```json
{
  "name": "my_package",
  "version": "1.0.0",
  "dependencies": {
    "my_dep": "^1.0.0"
  },
  "devDependencies" : {
    "my_test_framework": "^3.1.0"
  }
}
```

### install命令的--save和--save-dev参数

在`package.json`中添加依赖，比较简单和明智的方式是通过命令行，使用`npm install`命令和`--save`或者`--save-dev`参数。

添加到`dependencies`中：
```bash
> npm install <package_name> --save
```

添加到`devDependencies`中：
```bash
> npm install <package_name> --save-dev
```

### 管理依赖版本

npm使用`语义化版本号`来管理包的版本和版本范围。

如果在你的目录中有一个`package.json`文件，你运行`npm install`命令，npm会找到文件中列出的依赖包，并根据`语义化版本规则`下载最新的版本。

了解更多语义化版本内容，[看这里](https://docs.npmjs.com/getting-started/semantic-versioning)。
















