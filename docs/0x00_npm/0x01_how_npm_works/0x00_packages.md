# Packages

深入一个生态系统的关键之一就是了解它的词汇。包(`packages`)和模块(`modules`)很容易被搞混，NodeJS和npm对此有专门的定义。我们会讨论这些定义，使它们清晰，并且解释为什么某些文件的命名方式是这样的。

## 摘要

- `包(package)`是被`package.json`描述的一个文件或者目录。这有很多种不同的方式。更多内容可以看下面的[包是什么](#什么是包)。
- `模块(module)`是被NodeJS使用`require`加载的任意文件或者目录。同样，这样有多种配置。更多内容看下面的[模块是什么](#什么是模块)。

## 什么是包

包符合：
- a) 包含了一个被`package.json`描述的项目的目录
- b) (a)压缩包
- c) (b)的url链接
- d) 发布在npm registry上，\<name\>@\<version\>的形式
- e) \<name\>@\<tag\>，指向(d)
- f) \<name\>，但是有个latest的标签
- g) git url, 当clone下来的时候，满足(a)

注意这些包的所有可能性，即使你没有把包发布到npm registry上，使用npm依然可以有很多好处：
- 你仅仅是写一个node程序
- 如果你想压缩后到处都很容易的安装

git url可以是下面的形式：
```
git://github.com/user/project.git#commit-ish
git+ssh://user@hostname:project.git#commit-ish
git+http://user@hostname/project/blah.git#commit-ish
git+https://user@hostname/project/blah.git#commit-ish
```
`commit-ish`可以是任意的tag，sha，或者branch，`git checkout`能够接受的参数都可以。默认是`master`。

## 什么是模块

模块是在NodeJS程序中能够被`require()`加载的任何东西。下面是一些例子：
- 文件夹，包含了一个`package.json`文件并且有`main`属性的定义
- 文件夹，包含了一个`index.js`文件
- JS文件

### 大多数npm包是模块

通常，NodeJS程序中的用到的npm包是作为模块用`require`加载。

有些包，例如`cli`包，仅仅是包含了一个可执行的命令行接口，并没有提供一个`main`方法用来公NodeJS程序使用。这些包不是模块。

通常所有的npm包（至少在Node程序中）包含大量的模块（因为使用`require`加载的每个文件都是一个模块）。

在NodeJS程序的上下文中，模块也是被从文件中加载的内容。例如：
```javascript
var req = require('request')
```
我们可以这样说：变量`req`指向了模块`request`。

## NodeJS中的文件/目录名和npm生态

比如，为什么叫`node_modules`和`package.json`？为什么不叫`node_packages`和`module.json`？

`package.json`文件是定义包的。

`node_modules`文件夹是NodeJS查找模块的地方。

例如：如果你创建了一个文件`node_modules/foo.js`，然后有一个程序`var f = require('foo.js')`，它将加载这个模块。这个场景下，`foo.js`不是包，只是一个模块，因为并没有`package.json`文件。

相反，如果你创建了一个`package`，但是它没有`index.js`并且`package.json`中没有`main`字段，那么它就不是一个`module`。即使它被安装在了`node_modules`中，它也不可能作为`require()`的参数。
