# [译]EditorConfig文档说明

> http://editorconfig.org

## EditorConfig是什么
EditorConfig帮助开发者在不同的编辑器和IDE之间定义和维护统一的代码风格。EditorConfig项目核心是一个定义代码风格的版式文件，另外提供了一系列的插件，能够使编辑器读懂版式文件并遵循定义的样式。EditorConfig版式文件简单易懂，而且对VCS（版本控制系统）很友好。

## 版式文件

### 样例
下面有个版本文件`.editorconfig`的样例，该样例为Javascript和Python文件设置了行结束符（LF）和缩进风格。

```editorconfig
# EditorConfig is awesome: http://EditorConfig.org

# top-most EditorConfig file
root = true

# Unix-style newlines with a newline ending every file
[*]
end_of_line = lf
insert_final_newline = true

# Matches multiple files with brace expansion notation
# Set default charset
[*.{js,py}]
charset = utf-8

# 4 space indentation
[*.py]
indent_style = space
indent_size = 4

# Tab indentation (no size specified)
[Makefile]
indent_style = tab

# Indentation override for all JS under lib directory
[lib/**.js]
indent_style = space
indent_size = 2

# Matches the exact files either package.json or .travis.yml
[{package.json,.travis.yml}]
indent_style = space
indent_size = 2
```

可以通过查看WIKI来看看[使用EditorConfig真实案例项目](https://github.com/editorconfig/editorconfig/wiki/Projects-Using-EditorConfig)的配置。

### 版本文件位置

当打开文件的时候，EditorConfig插件会查找一个名为`.editorconfig`的文件，查找路径是从当前文件所在目录下开始，往上级目录找，直到找到根路径或者遇到版式文件中包含`root=true`的文件。

EditorConfig会将找到的版本文件，自顶层到最下层读取，因此，离打开文件最近的版本文件最后读。EditorConfig读取版本文件，匹配到打开文件所属的配置项时，按照读取顺序使用，这样的话，后面的配置会覆盖前面的配置，因此距离文件最近的版本文件优先级就越高。

**Windows用户:** 在Windows文件夹中，直接创建`.editorconfig`文件是不允许的，所以你可以创建一个名为`.editorconfig.`的文件（末尾多了一个`.`），Windows会自动重命名为`.editorconfig`的。

### 版式文件详情

EditorConfig使用是兼容于[Python ConfigParser Library](https://docs.python.org/2/library/configparser.html)的`INI`配置格式，但是在`section`名称中允许出现`[`和`]`。`section`名称是文件路径的[globs](https://en.wikipedia.org/wiki/Glob_(programming)),类似于[gitignore](http://manpages.ubuntu.com/manpages/intrepid/man5/gitignore.5.html#contenttoc2)中的格式。斜杠（`/`）作为路径分隔符，`#`或者分号(`;`)用来注释。注释仅作用于所在行。EditorConfig版式文件编码为`UTF-8`，行结束符`CRLF`和`LF`都可以。

下表为文件路径`glob`表达式和当前支持的的EditorConfig配置项。

- **通配表达式**

`section`名称中能够识别的特定字符串：

---
    *               匹配任意字符串（`/`除外）
---
    **              匹配任意字符串
---
    ?               匹配任意单个字符
---
    [name]          匹配name中的任意单个字符
---
    [!name]         匹配不在name中的任意单个字符
---
    {s1, s2, s3}    匹配给定的任意字符串（以`,`分隔）（EditorConfig Core 0.11.0版本及以上）
---
    {num1...num2}   匹配在`num1`到`num2`间的任意整型数值，`num1`和`num2`可以是正也可以是负

特定的字符串可以使用反斜杠（` \ `）来转义，这样的话，就不会作为一个通配符使用。

- **支持的属性**

需要注意的是，并非所有的属性都被每一个插件所支持（[属性完整清单](https://github.com/editorconfig/editorconfig/wiki/EditorConfig-Properties)）。

**indent_style**: 设置为`tab`或者`space`，从而使用tab或者空格来缩进。
**indent_size**: 如果缩进使用空格的话，这里的值就是指单个缩进级别的空格数，如果是使用tab的话，就是使用`tab_width`的值。
**tab_width**: 当缩进使用`tab`方式时，单个`tab`的单位宽度。该属性的默认值为`indent_size`的值，所以不需要特别指定。
**end_of_line**: 行结束符，可以设置为`lf`，`cr`，`crlf`。
**charset**: 字符集，可以设置为`latin1`，`utf-8`，`utf-8-bom`，`utf-16be`，`utf-16le`。[不推荐使用`utf-8-bom`](https://stackoverflow.com/questions/2223882/whats-different-between-utf-8-and-utf-8-without-bom/2223926#2223926)。
**trim_trailing_whitespace**: 设置为`true`或`false`，设置为`true`时，删除换行符前面的空白字符，设置为`false`的话，不会这样做。
**insert_final_newline**: 设置为`true`或`false`，`true`时，会保证文件会以一个新行结束，`false`保证不会。
**root**: 特殊的属性，应该在文件的最上面，且不在任何的`section`中，该属性会终止对`.editorconfig`进一步查找。

目前所有的属性和值都是`大小写不敏感`的。在解析的时候，它们都是小写的。通常，如果某个属性没有设置，会使用编辑器本身的设置，EditorConfig不会对这一方面产生影响。

EditorConfig某些属性不配置是可以被接收的，通常也是这么做的。例如，`tab_width`是不需要指定的，除非它跟`indent_size`不一致。而且，当`indent_style`设置成`tab`的时候，`indent_size`可能会不设置，这样的话，读者可以用他们喜欢的宽度来看这些文件。此外，在你的项目中如果某些属性（如`end_of_line`）没有标准的话，最好不设置。

对于任何属性来说，值为`unset`的话会消除该属性的作用，即使之前已经配置过。例如，添加`indent_size = unset`会取消`indent_size`配置（使用编辑器默认）。
