# TODO List

- [ ] Project Structure
- [ ] docs
    - [ ] npm
        - [ ] Getting Started
            - [ ] 01 What is npm
            - [x] 05 Using a `package.json`
            - [x] 13 Semantic versioning and npm
        - [ ] How npm works
            - [x] 01 Packages
            - [ ] 02 npm v2
            - [ ] 03 npm v3
            - [ ] 04 npm v3 Duplication
            - [ ] 05 npm v3 Non-determinism
    - [ ] EditorConfig
        - [x] editorconfig文档
    - [ ] git
        - [x] gitignore文档
    - [ ] webpack
